﻿using Ninject.Modules;
using VKR.Controllers.Data;
using VKR.Controllers.Data.Interfaces;
using VKR.Controllers.Logic;
using VKR.Controllers.Logic.Interfaces;
using VKR.Controllers.UI;
using VKR.Database.Interfaces;
using VKR.Database.Repositories;
using VKR.Database.UnitOfWork;

namespace VKR.IoC
{
    public class IoCMappingModule  : NinjectModule
    {
        public override void Load()
        {
            Bind<IProjectDbModelRepository>().To<ProjectDbModelRepository>();
            Bind<IResultDbModelRepository>().To<ResultDbModelRepository>();
            Bind<ITestDbModelRepository>().To<TestDbModelRepository>();

            Bind<IUnitOfWork>().To<UnitOfWork>();

            Bind<ITestDbModelDataController>().To<TestDbModelDataController>();
            Bind<IResultDbModelDataController>().To<ResultDbModelDataController>();
            Bind<IProjectDbModelDataController>().To<ProjectDbModelDataController>();

            Bind<ILoadTestController>().To<LoadTestController>();
            Bind<IPerformTestController>().To<PerformTestController>();
            Bind<IReportController>().To<ReportController>();
            Bind<ISettingController>().To<SettingController>();
            Bind<IStatisticController>().To<StatisticController>();
            Bind<IWebDriverController>().To<WebDriverController>();

            Bind<IInfoStackPanelBuilder>().To<InfoStackPanelBuilder>();
        }
    }
}
