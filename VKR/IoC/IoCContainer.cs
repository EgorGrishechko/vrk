﻿using Ninject;
using Ninject.Modules;

namespace VKR.IoC
{
    public static class IoCContainer
    {
        private static KernelBase _kernel = new StandardKernel();

        public static KernelBase Kernel
        {
            get { return _kernel; }
        }

        public static void Initialize(params INinjectModule[] modules)
        {
            _kernel.Load(modules);
        }
    }
}
