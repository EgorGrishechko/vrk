﻿using System;
using System.Collections.Generic;

namespace VKR.Models.Test
{
    public class UploadedTestModel
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public DateTime? Date { get; set; }
        public List<UploadedTestStepModel> Steps { get; set; } 
    }
}
