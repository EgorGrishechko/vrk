﻿namespace VKR.Models.Test
{
    public class UploadedTestStepModel
    {
        public string Action { get; set; }
        public string ElementAttributes { get; set; }
        public string ElementCssSelector { get; set; }
        public string ElementValue { get; set; }
        public int? WaitBefore { get; set; }
        public int? WaitAfter { get; set; }
    }
}
