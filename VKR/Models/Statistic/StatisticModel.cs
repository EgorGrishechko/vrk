﻿using System.Collections.Generic;

namespace VKR.Models.Statistic
{
    public class StatisticModel
    {
        public double SuccessedPercent { get; set; }
        public double FailedPercent { get; set; }
        public Dictionary<string, double> FailedProbability { get; set; } 
    }
}
