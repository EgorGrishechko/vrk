﻿namespace VKR.Models.WebDriver
{
    public class WebItemModel
    {
        public string Id;
        public string ClassName;
        public string XPathQuery;

        public WebItemModel(string id, string className, string xPathQuery)
        {
            Id = id;
            ClassName = className;
            XPathQuery = xPathQuery;
        }
    }
}
