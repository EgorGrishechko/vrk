﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VKR.Database.Context;
using VKR.Database.Interfaces;
using VKR.Database.Models;

namespace VKR.Database.Repositories
{
    public class ResultDbModelRepository : BaseRepository<ResultDbModel>, IResultDbModelRepository
    {
        public ResultDbModelRepository(DbContext context) : base(context)
        {
        }

        public async Task<IList<ResultDbModel>> GetByTestIdAsync(Guid id)
        {
            var result = await Search(r => r.Test.Id == id).ToListAsync();
            return result;
        }

        public async Task<ResultDbModel> GetLastResultAsync()
        {
            var result = await GetAll().OrderByDescending(r => r.Date).FirstOrDefaultAsync();
            return result;
        }

        public async Task<IList<ResultDbModel>> GetResultsByDateWithoutTimeAsync(DateTime date)
        {
            var result = await Search(r => r.Date.Year == date.Year && r.Date.Month == date.Month && r.Date.Day == date.Day).ToListAsync();
            return result;
        }

        public async Task<IList<ResultDbModel>> GetResultsByDateTimeAsync(DateTime date)
        {
            var result = await Search(r => r.Date == date).ToListAsync();
            return result;
        }
    }
}
