﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VKR.Database.Context;
using VKR.Database.Interfaces;
using VKR.Database.Models;

namespace VKR.Database.Repositories
{
    public class ProjectDbModelRepository : BaseRepository<ProjectDbModel>, IProjectDbModelRepository
    {
        public ProjectDbModelRepository(DbContext context) : base(context)
        {
        }

        public async Task<ProjectDbModel> GetByUrlAsync(string url)
        {
            var result = await Search(p => p.Url == url).FirstOrDefaultAsync();
            return result;
        }
    }
}
