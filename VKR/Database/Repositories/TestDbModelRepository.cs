﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using VKR.Database.Context;
using VKR.Database.Interfaces;
using VKR.Database.Models;
using VKR.Resources;

namespace VKR.Database.Repositories
{
    public class TestDbModelRepository : BaseRepository<TestDbModel>, ITestDbModelRepository
    {
        public TestDbModelRepository(DbContext context) : base(context)
        {
        }

        public async Task<IList<TestDbModel>> GetSuccessedResultTestsAsync()
        {
            var tests = await
                Search(t => t.Results.All(r => r.Status == TestStatusEnum.Passed)).ToListAsync();
            return tests;
        }

        public async Task<IList<TestDbModel>> GetFailedResultTestsAsync()
        {
            var tests = await
                Search(t => t.Results.All(r => r.Status == TestStatusEnum.Failed)).ToListAsync();
            return tests;
        }

        public async Task<IList<TestDbModel>> GetCrossResultTestsAsync()
        {
            var tests = await
                Search(t => t.Results.Count(r => r.Status == TestStatusEnum.Failed) > 0
                                    && t.Results.Count(r => r.Status == TestStatusEnum.Passed) > 0).ToListAsync();
            return tests;
        }
    }
}
