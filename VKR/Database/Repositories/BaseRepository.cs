﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VKR.Database.Context;
using VKR.Database.Interfaces;
using VKR.Database.Models;

namespace VKR.Database.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseDbModel
    {
        private readonly DbContext _context;
        protected DbSet<TEntity> DbSet;

        protected BaseRepository(DbContext context)
        {
            _context = context;
            DbSet = _context.Set<TEntity>(); 
        }

        public virtual async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await DbSet.Where(e => e.Id == id).FirstOrDefaultAsync();
        }

        public virtual async Task InsertAsync(TEntity obj)
        {
            DbSet.Add(obj);
            await SaveChangesAsync();
        }

        public virtual async Task RemoveAsync(Guid id)
        {
            var deletingEntity = await GetByIdAsync(id);
            DbSet.Remove(deletingEntity);
            await SaveChangesAsync();
        }

        public virtual async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public virtual async Task<IList<TEntity>> GetListAsync()
        {
            return await DbSet.ToListAsync();
        }

        public virtual IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }
    }
}
