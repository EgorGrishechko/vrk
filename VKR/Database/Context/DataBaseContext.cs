﻿using System.Data.Entity;
using VKR.Database.Models;

namespace VKR.Database.Context
{
    public class DataBaseContext :  DbContext
    {
        public DataBaseContext() : base("name=DataBaseContext") { }

        public DbSet<TestDbModel> Tests { get; set; }
        public DbSet<ProjectDbModel> Projects { get; set; }
        public DbSet<ResultDbModel> Results { get; set; }
    }
}
