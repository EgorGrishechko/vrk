﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Parameters;
using VKR.Database.Context;
using VKR.Database.Interfaces;
using VKR.IoC;

namespace VKR.Database.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext _context = new DataBaseContext();

        private IResultDbModelRepository _resultDbModelRepository;
        private ITestDbModelRepository _testDbModelRepository;
        private IProjectDbModelRepository _projectDbModelRepository;

        public UnitOfWork()
        {
            _resultDbModelRepository = IoCContainer.Kernel.Get<IResultDbModelRepository>(new ConstructorArgument("context", _context));
            _testDbModelRepository = IoCContainer.Kernel.Get<ITestDbModelRepository>(new ConstructorArgument("context", _context));
            _projectDbModelRepository = IoCContainer.Kernel.Get<IProjectDbModelRepository>(new ConstructorArgument("context", _context));
        }

        public IResultDbModelRepository ResultDbModelRepository
        {
            get { return _resultDbModelRepository; }
        }

        public ITestDbModelRepository TestDbModelRepository
        {
            get { return _testDbModelRepository; }
        }

        public IProjectDbModelRepository ProjectDbModelRepository
        {
            get { return _projectDbModelRepository; }
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
