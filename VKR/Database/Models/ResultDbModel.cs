﻿using System;
using System.ComponentModel.DataAnnotations;
using VKR.Resources;

namespace VKR.Database.Models
{
    public class ResultDbModel : BaseDbModel
    {
        public virtual TestDbModel Test { get; set; }
        public String FailureReason { get; set; }
        public TestStatusEnum Status { get; set; }
        public DateTime Date { get; set; }
    }
}
