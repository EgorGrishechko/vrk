﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VKR.Database.Models
{
    public class ProjectDbModel : BaseDbModel
    {
        public string Url { get; set; }
        public virtual ICollection<TestDbModel> Tests { get; set; }
    }
}
