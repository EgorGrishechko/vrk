﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VKR.Database.Models
{
    public class TestDbModel : BaseDbModel
    {
        public String Name { get; set; }
        public DateTime Date { get; set; }
        public virtual ProjectDbModel Project { get; set; }
        public virtual ICollection<ResultDbModel> Results { get; set; }
    }
}
