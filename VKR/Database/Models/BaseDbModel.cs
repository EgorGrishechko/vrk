﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKR.Database.Models
{
    public abstract class BaseDbModel
    {
        [Key]
        public Guid Id { get; set; }
    }
}
