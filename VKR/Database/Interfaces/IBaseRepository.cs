﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace VKR.Database.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class 
    {
        Task<TEntity> GetByIdAsync(Guid id);
        Task InsertAsync(TEntity obj);
        Task RemoveAsync(Guid id);
        Task SaveChangesAsync();
        Task<IList<TEntity>> GetListAsync();

        IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> GetAll(); 
    }
}
