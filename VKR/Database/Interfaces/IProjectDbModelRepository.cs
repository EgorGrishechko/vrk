﻿using System;
using System.Threading.Tasks;
using VKR.Database.Models;

namespace VKR.Database.Interfaces
{
    public interface IProjectDbModelRepository : IBaseRepository<ProjectDbModel>
    {
        Task<ProjectDbModel> GetByUrlAsync(String url);
    }
}
