﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Database.Models;

namespace VKR.Database.Interfaces
{
    public interface IResultDbModelRepository : IBaseRepository<ResultDbModel>
    {
        Task<IList<ResultDbModel>> GetByTestIdAsync(Guid id);
        Task<ResultDbModel> GetLastResultAsync();
        Task<IList<ResultDbModel>> GetResultsByDateWithoutTimeAsync(DateTime date);
        Task<IList<ResultDbModel>> GetResultsByDateTimeAsync(DateTime date);
    }
}
