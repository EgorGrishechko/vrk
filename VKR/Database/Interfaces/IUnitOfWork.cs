﻿using System;

namespace VKR.Database.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IResultDbModelRepository ResultDbModelRepository { get; }
        ITestDbModelRepository TestDbModelRepository { get; }
        IProjectDbModelRepository ProjectDbModelRepository { get; }
    }
}
