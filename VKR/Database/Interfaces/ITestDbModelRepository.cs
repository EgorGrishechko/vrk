﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Database.Models;

namespace VKR.Database.Interfaces
{
    public interface ITestDbModelRepository : IBaseRepository<TestDbModel>
    {
        Task<IList<TestDbModel>> GetSuccessedResultTestsAsync();
        Task<IList<TestDbModel>> GetFailedResultTestsAsync();
        Task<IList<TestDbModel>> GetCrossResultTestsAsync();
    }
}
