﻿using System.Windows;
using VKR.Frames;
using VKR.IoC;

namespace VKR
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            IoCContainer.Initialize(new IoCMappingModule());

            MainFrame.Navigate(new MainMenuFrame(MainFrame));
        }

        private void MenuItemTest_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new TestsFrame(MainFrame));
        }

        private void MenuItemStatistic_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new StatisticFrame(MainFrame));
        }

        private void MenuItemSettings_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(new SettingsFrame(MainFrame));
        }
    }
}
