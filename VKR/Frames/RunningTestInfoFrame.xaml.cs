﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Ninject;
using Ninject.Parameters;
using VKR.Controllers.Logic.Interfaces;
using VKR.Controllers.UI;
using VKR.Database.Interfaces;
using VKR.IoC;
using VKR.Models.Test;

namespace VKR.Frames
{
    public partial class RunningTestInfoFrame : Page
    {
        private Frame _parentFrame;
        private List<UploadedTestModel> _selectedTests;
        public bool isConstructed;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IInfoStackPanelBuilder _infoStackPanelBuilder;
        private readonly IPerformTestController _performTestController;

        public RunningTestInfoFrame(Frame parentFrame, List<UploadedTestModel> selectedTests)
        {
            InitializeComponent();
            _parentFrame = parentFrame;
            _selectedTests = selectedTests;
            isConstructed = true;

            _infoStackPanelBuilder = IoCContainer.Kernel.Get<IInfoStackPanelBuilder>();
            _unitOfWork = IoCContainer.Kernel.Get<IUnitOfWork>();
            _performTestController = IoCContainer.Kernel.Get<IPerformTestController>(new ConstructorArgument("unitOfWork", _unitOfWork));
        }


        private async void RunningTestInfoFrame_OnLoaded(object sender, RoutedEventArgs e)
        {
            if (isConstructed)
            {
                isConstructed = false;
                string mainMessage = "\nId - {0}\n Имя - {1}";

                if (_selectedTests.Count == 0)
                {
                    _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, "Нет выбранных тестов!");
                    return;
                }

                var testingDate = DateTime.Now;
                foreach (var selectedTest in _selectedTests)
                {
                    if (!selectedTest.Id.HasValue)
                    {
                        _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, String.Format(mainMessage, new Guid(), selectedTest.Name));
                        _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, "Отсутствует ID.");
                        continue;
                    }

                    string testInfo = String.Format(mainMessage, selectedTest.Id.Value, selectedTest.Name);

                    if (selectedTest.Steps == null || selectedTest.Steps.Count == 0)
                    {
                        _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, testInfo);
                        _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, "Нет действий.");
                        continue;
                    }

                    if (selectedTest.Steps[0].Action != "Открыть страницу")
                    {
                        _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, testInfo);
                        _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, "Первый шаг должен быть Открыть страницу");
                        continue;
                    }

                    _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, testInfo);
                    _infoStackPanelBuilder.AddTextBlockToInfoPanel(RunningTestInfoPanel, "Выполнение начато");

                    await _performTestController.PerfomTest(selectedTest, RunningTestInfoPanel, testingDate);
                }
            }
        }
    }
}
