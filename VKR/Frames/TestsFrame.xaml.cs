﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ninject;
using VKR.Controllers.Logic.Interfaces;
using VKR.IoC;
using VKR.Models.Test;

namespace VKR.Frames
{
    public partial class TestsFrame : Page
    {
        private Frame _parentFrame;
        private readonly ILoadTestController _loadTestController;

        public TestsFrame(Frame parentFrame)
        {
            InitializeComponent();
            _parentFrame = parentFrame;

            _loadTestController = IoCContainer.Kernel.Get<ILoadTestController>();
        }


        private async void LoadTestsBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var result = await _loadTestController.LoadTests();
            if (result != null && result.Any()) ShowTestLstBox.ItemsSource = result;
            else MessageBox.Show("Отсутсвуют тесты для загрузки", "Ошибка");
        }

        private void RunTestsBtn_OnClick(object sender, RoutedEventArgs e)
        {
            var selectedTests = ShowTestLstBox.SelectedItems.Cast<UploadedTestModel>().ToList();
            _parentFrame.Navigate(new RunningTestInfoFrame(_parentFrame, selectedTests));
        }
    }
}
