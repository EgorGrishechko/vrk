﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Ninject;
using Ninject.Parameters;
using VKR.Controllers.Data.Interfaces;
using VKR.Controllers.Logic.Interfaces;
using VKR.Database.Interfaces;
using VKR.Database.Models;
using VKR.IoC;

namespace VKR.Frames
{
    public partial class StatisticFrame : Page
    {
        private Frame _parentFrame;
        private DateTime? _currentDate;
        private IList<ResultDbModel> _resultsSelectedByTest;
        private IList<ResultDbModel> _resultsSelectedByDate;
        private IList<TestDbModel> _savedTest;

        private readonly ITestDbModelDataController _testDbModelDataController;
        private readonly IResultDbModelDataController _resultDbModelDataController;
        private readonly IReportController _reportController;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStatisticController _statisticController;

        public StatisticFrame(Frame parentFrame)
        {
            InitializeComponent();
            _parentFrame = parentFrame;
            _unitOfWork = IoCContainer.Kernel.Get<IUnitOfWork>();
            _testDbModelDataController = IoCContainer.Kernel.Get<ITestDbModelDataController>(new ConstructorArgument("unitOfWork", _unitOfWork));
            _resultDbModelDataController = IoCContainer.Kernel.Get<IResultDbModelDataController>(new ConstructorArgument("unitOfWork", _unitOfWork));
            _reportController = IoCContainer.Kernel.Get<IReportController>();
            _statisticController = IoCContainer.Kernel.Get<IStatisticController>(new ConstructorArgument("unitOfWork", _unitOfWork));
        }



        private async void DatePicker_OnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            TimerPicker.Visibility = Visibility.Hidden;
            TimerPicker.Text = "Выберите время";
            TimerPicker.ItemsSource = null;
            var selectedDate = e.AddedItems[0] as DateTime?;
            _currentDate = selectedDate;
            if (selectedDate.HasValue)
            {
                var times = await _resultDbModelDataController.GetResultTimesByDateWithOutTimeAsync(selectedDate.Value);
                TimerPicker.ItemsSource = times;
                TimerPicker.Visibility = Visibility.Visible;
            }
        }


        private async void TimerPicker_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedTime = e.AddedItems[0] as TimeSpan?;
            if (selectedTime.HasValue)
            {
                var pointDate = _currentDate.Value.Add(selectedTime.Value);
                var selectedResults = await _resultDbModelDataController.GetResultsByDateTimeAsync(pointDate);
                _resultsSelectedByDate = selectedResults;
                ResultGrid.ItemsSource = selectedResults;
            }
        }

 
        private async void TestPicker_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedTest = e.AddedItems[0] as TestDbModel;
            if (selectedTest != null)
            {
                var selectedResults = await _resultDbModelDataController.GetByTestIdAsync(selectedTest.Id);
                _resultsSelectedByTest = selectedResults;
                TestResultGrid.ItemsSource = selectedResults;
            }            
        }

        private void TestResultsReport_OnClick(object sender, RoutedEventArgs e)
        {
            if (_resultsSelectedByTest != null && _resultsSelectedByTest.Count > 0)
            {
                _reportController.CreateTestReportByTestPerforming(_resultsSelectedByTest);
            }
            else
            {
                MessageBox.Show("Отсутсвуют данные для отчета", "Ошибка");
            }
        }

        private void DateResultsReport_OnClick(object sender, RoutedEventArgs e)
        {
            if (_resultsSelectedByDate != null && _resultsSelectedByDate.Count > 0)
            {
                _reportController.CreateTestReportByDate(_resultsSelectedByDate);                
            }
            else
            {
                MessageBox.Show("Отсутсвуют данные для отчета", "Провал");                
            }
        }

        private async void TestTab_OnGotFocus(object sender, RoutedEventArgs e)
        {

            if (_savedTest == null)
            {
                _savedTest = await _testDbModelDataController.GetListAsync();
            }

            TestPicker.ItemsSource = _savedTest;
        }

        private async void AnalysisTab_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (_savedTest == null)
            {
                _savedTest = await _testDbModelDataController.GetListAsync();
            }

            AnalysTestPicker.ItemsSource = _savedTest;            
        }

        private async void AnalysTestPicker_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedTest = e.AddedItems[0] as TestDbModel;
            if (selectedTest != null)
            {
                var statisticModel = await _statisticController.CalculateProbability(selectedTest);
                if (statisticModel != null)
                {
                    SetAnalysticFields(statisticModel.SuccessedPercent.ToString(CultureInfo.InvariantCulture), 
                        statisticModel.FailedPercent.ToString(CultureInfo.InvariantCulture), 
                        statisticModel.FailedProbability);
                }
                else
                {
                    MessageBox.Show("Невозможно посчитать. Отсутствуют данные", "Ошибка");
                    SetAnalysticFields(String.Empty, String.Empty, null);
                }
            }
        }

        private void SetAnalysticFields(string successedPercentage, string failedPercentage,
            Dictionary<string, double> failedProbability)
        {
            SuccessedPercentageTxtBx.Text = successedPercentage;
            FailedPercentageTxtBx.Text = failedPercentage;
            FailureProbabilityLstBx.ItemsSource = failedProbability;
        }

        private void ModeSelecter_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var currentValue = e.NewValue;
            if (currentValue == 1)
            {
                ProbabilityGrid.Visibility = Visibility.Collapsed;
                SurveyGrid.Visibility = Visibility.Visible;
            }
            else
            {
                ProbabilityGrid.Visibility = Visibility.Visible;
                SurveyGrid.Visibility = Visibility.Collapsed;
            }
        }

        private async void SurveyModePicker_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedSurveyMode = e.AddedItems[0] as ComboBoxItem;
            if (selectedSurveyMode != null && selectedSurveyMode.Content != null)
            {
                var tests = await _statisticController.GetTestBySurveyType(selectedSurveyMode.Content.ToString());
                if (tests == null || tests.Count == 0)
                    MessageBox.Show("Нет тестов подходящих для данного режима", "Ошибка");
                else SurveyModeGridForTest.ItemsSource = tests;
            }    
        }
    }
}
