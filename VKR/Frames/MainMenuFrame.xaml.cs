﻿using System.Windows;
using System.Windows.Controls;
using Ninject;
using Ninject.Parameters;
using VKR.Controllers.Data.Interfaces;
using VKR.Controllers.UI;
using VKR.Database.Interfaces;
using VKR.IoC;

namespace VKR.Frames
{
    public partial class MainMenuFrame : Page
    {
        private Frame _parentFrame;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IResultDbModelDataController _resultDbModelDataController;
        private readonly IInfoStackPanelBuilder _infoStackPanelBuilder;


        public MainMenuFrame(Frame parentFrame)
        {
            InitializeComponent();
            _parentFrame = parentFrame;

            _unitOfWork = IoCContainer.Kernel.Get<IUnitOfWork>();
            _resultDbModelDataController = IoCContainer.Kernel.Get<IResultDbModelDataController>(new ConstructorArgument("unitOfWork", _unitOfWork));
            _infoStackPanelBuilder = IoCContainer.Kernel.Get<IInfoStackPanelBuilder>();
        }

        private async void MainMenuFrame_OnLoaded(object sender, RoutedEventArgs e)
        {
            var lastResult = await _resultDbModelDataController.GetLastResultAsync();
            _infoStackPanelBuilder.AddLastResultBlockToInfoPanel(LastTestingInfo, lastResult);
        }
    }
}
