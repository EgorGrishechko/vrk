﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Ninject;
using VKR.Controllers.Logic.Interfaces;
using VKR.IoC;

namespace VKR.Frames
{
    /// <summary>
    /// Interaction logic for SettingsFrame.xaml
    /// </summary>
    public partial class SettingsFrame : Page
    {
        private Frame _parentFrame;
        private ISettingController _settingController;

        public SettingsFrame(Frame parentFrame)
        {
            InitializeComponent();
            _parentFrame = parentFrame;

            _settingController = IoCContainer.Kernel.Get<ISettingController>();

            string reportFilesDirectoryPath = _settingController.GetReportPath();
            string testFilesDirectoryPath = _settingController.GetTestPath();

            TestPathTxtBox.Text = testFilesDirectoryPath;
            ReportPathTxtBox.Text = reportFilesDirectoryPath;
        }

        private void SaveSettings_Click(object sender, RoutedEventArgs e)
        {
            var testFilesDirectoryPath = TestPathTxtBox.Text;
            var reportFilesDirectoryPath = ReportPathTxtBox.Text;
            if (!String.IsNullOrEmpty(testFilesDirectoryPath) && !String.IsNullOrEmpty(reportFilesDirectoryPath))
            {
                var result = MessageBox.Show("Вы уверены?", "Сохранить", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    _settingController.SetReportPath(reportFilesDirectoryPath);
                    _settingController.SetTestPath(testFilesDirectoryPath);
                }
            }
            else
            {
                MessageBox.Show("Одной из полей пустое.", "Ошибка");
            }
        }

        private void GenerateIdTxtBox_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            GenerateIdTxtBox.Text = Guid.NewGuid().ToString();
        }
    }
}
