﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VKR.Database.Interfaces;

namespace VKR.Controllers.Base.Interfaces
{
    public interface IBaseController : IDisposable
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
