﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VKR.Controllers.Base.Interfaces;
using VKR.Database.Interfaces;

namespace VKR.Controllers.Base
{
    public class BaseController : IBaseController
    {
        private readonly IUnitOfWork _unitOfWork;

        public BaseController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IUnitOfWork UnitOfWork {
            get
            {
                return _unitOfWork;
            }}

        public void Dispose()
        {
            UnitOfWork.Dispose();
        }
    }
}
