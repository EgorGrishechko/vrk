﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using VKR.Database.Models;
using VKR.Resources;

namespace VKR.Controllers.UI
{
    public class InfoStackPanelBuilder : IInfoStackPanelBuilder
    {
        public void AddTextBlockToInfoPanel(StackPanel parentStackPanel, String message)
        {
            var infoMessagePanel = CreateInfoStackPanel(message);
            parentStackPanel.Children.Add(infoMessagePanel);
        }

        public StackPanel CreateInfoStackPanel(string message)
        {
            var parentStackPanel = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(5, 0, 0, 0) };
            var childTextBlock = new TextBlock { Text = message };
            parentStackPanel.Children.Add(childTextBlock);
            return parentStackPanel;
        }

        public StackPanel CreateEmptyInfoStackPanel()
        {
            return CreateInfoStackPanel(" ");
        }

        public void AddLastResultBlockToInfoPanel(StackPanel stackPanel, ResultDbModel lastSavedResult)
        {
            if (lastSavedResult != null)
            {
                var lastRunnedTest = lastSavedResult.Test;

                AddTextBlockToInfoPanel(stackPanel, String.Format("Тест Id - {0}", lastRunnedTest.Id));
                AddTextBlockToInfoPanel(stackPanel, String.Format("Имя теста - {0}", lastRunnedTest.Name));
                AddTextBlockToInfoPanel(stackPanel, String.Format("Статус - {0}", lastSavedResult.Status == TestStatusEnum.Passed ? "Успешен" : "Провален"));

                if (lastSavedResult.Status == TestStatusEnum.Failed)
                {
                    AddTextBlockToInfoPanel(stackPanel, String.Format("Причина провала - {0}", lastSavedResult.FailureReason));
                }

                AddTextBlockToInfoPanel(stackPanel, String.Format("Дата тестирования - {0}", lastSavedResult.Date));
            }
        }
    }
}
