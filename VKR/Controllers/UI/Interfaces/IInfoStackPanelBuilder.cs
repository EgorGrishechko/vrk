﻿using System;
using System.Windows.Controls;
using VKR.Database.Models;

namespace VKR.Controllers.UI
{
    public interface IInfoStackPanelBuilder
    {
        void AddTextBlockToInfoPanel(StackPanel parentStackPanel, String message);
        StackPanel CreateInfoStackPanel(string message);
        StackPanel CreateEmptyInfoStackPanel();
        void AddLastResultBlockToInfoPanel(StackPanel stackPanel, ResultDbModel lastSavedResult);
    }
}