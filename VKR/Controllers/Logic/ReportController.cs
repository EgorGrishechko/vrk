﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using Novacode;
using VKR.Controllers.Logic.Interfaces;
using VKR.Database.Models;

namespace VKR.Controllers.Logic
{
    public class ReportController : IReportController
    {
        public void CreateTestReportByTestPerforming(IList<ResultDbModel> results)
        {
            var fileName = CreateReportFilePath("TestReport_{0}.docx");

            using (var document = DocX.Create(fileName))
            {
                var fontFamily = new System.Drawing.FontFamily("Times New Roman");
                var fontSize = 14;

                var firstParagraph =
                    document.InsertParagraph(String.Format("Id теста: {0}", results[0].Test.Id));
                firstParagraph.Alignment = Alignment.center;
                firstParagraph.Font(fontFamily);
                firstParagraph.FontSize(fontSize);


                var secondParagraph =
                    document.InsertParagraph(String.Format("Название теста: {0}", results[0].Test.Name));
                secondParagraph.Alignment = Alignment.center;
                secondParagraph.Font(fontFamily);
                secondParagraph.FontSize(fontSize);

                var thirdParagraph =
                    document.InsertParagraph(String.Format("Дата создания: {0}", results[0].Test.Date.ToShortDateString()));
                thirdParagraph.Alignment = Alignment.center;
                thirdParagraph.Font(fontFamily);
                thirdParagraph.FontSize(fontSize);

                InsertEmptyParagraph(document, 2);

                var table = CreateTestReportTable(document, results);
                document.InsertTable(table);
                document.Save();
                MessageBox.Show("Отчет сохранен", "Успешно");
            }
        }

        public void CreateTestReportByDate(IList<ResultDbModel> results)
        {
            var fileName = CreateReportFilePath("DateReport_{0}.docx");

            using (var document = DocX.Create(fileName))
            {
                var fontFamily = new System.Drawing.FontFamily("Times New Roman");
                var fontSize = 14;

                var firstParagraph =
                    document.InsertParagraph(String.Format("Дата тестирования: {0}", results[0].Date));
                firstParagraph.Alignment = Alignment.center;
                firstParagraph.Font(fontFamily);
                firstParagraph.FontSize(fontSize);


                InsertEmptyParagraph(document, 2);

                var table = CreateDateReportTable(document, results);
                document.InsertTable(table);
                document.Save();
                MessageBox.Show("Отчет сохранен", "Успешно");
            }
        }

        private Table CreateTestReportTable(DocX document, IList<ResultDbModel> results)
        {
            var fontFamily = new FontFamily("Times New Roman");

            var table = document.AddTable(results.Count + 1, 3);
            table.Alignment = Alignment.center;

            ConfigureParagraph(table.Rows[0].Cells[0].Paragraphs.First(), "Дата тестирования", 12, Alignment.center, fontFamily);
            ConfigureParagraph(table.Rows[0].Cells[1].Paragraphs.First(), "Результат тестирования", 12, Alignment.center, fontFamily);
            ConfigureParagraph(table.Rows[0].Cells[2].Paragraphs.First(), "Причина провала", 12, Alignment.center, fontFamily);

            var rowIndex = 1;
            foreach (var result in results)
            {
                ConfigureParagraph(table.Rows[rowIndex].Cells[0].Paragraphs.First(), result.Date.ToString(), 12, Alignment.center, fontFamily);
                ConfigureParagraph(table.Rows[rowIndex].Cells[1].Paragraphs.First(), result.Status.ToString(), 12, Alignment.center, fontFamily);
                ConfigureParagraph(table.Rows[rowIndex].Cells[2].Paragraphs.First(), result.FailureReason, 12, Alignment.center, fontFamily);
                rowIndex++;
            }

            return table;
        }

        private Table CreateDateReportTable(DocX document, IList<ResultDbModel> results)
        {
            var fontFamily = new FontFamily("Times New Roman");

            var table = document.AddTable(results.Count + 1, 5);
            table.Alignment = Alignment.center;

            ConfigureParagraph(table.Rows[0].Cells[0].Paragraphs.First(), "Id", 12, Alignment.center, fontFamily);
            ConfigureParagraph(table.Rows[0].Cells[1].Paragraphs.First(), "Название", 12, Alignment.center, fontFamily);
            ConfigureParagraph(table.Rows[0].Cells[2].Paragraphs.First(), "Адрес проекта", 12, Alignment.center, fontFamily);
            ConfigureParagraph(table.Rows[0].Cells[3].Paragraphs.First(), "Статус тестирования", 12, Alignment.center, fontFamily);
            ConfigureParagraph(table.Rows[0].Cells[4].Paragraphs.First(), "Причина провала", 12, Alignment.center, fontFamily);

            var rowIndex = 1;
            foreach (var result in results)
            {

                ConfigureParagraph(table.Rows[rowIndex].Cells[0].Paragraphs.First(), result.Test.Id.ToString(), 12, Alignment.center, fontFamily);
                ConfigureParagraph(table.Rows[rowIndex].Cells[1].Paragraphs.First(), result.Test.Name, 12, Alignment.center, fontFamily);
                ConfigureParagraph(table.Rows[rowIndex].Cells[2].Paragraphs.First(), result.Test.Project.Url, 12, Alignment.center, fontFamily);
                ConfigureParagraph(table.Rows[rowIndex].Cells[3].Paragraphs.First(), result.Status.ToString(), 12, Alignment.center, fontFamily);
                ConfigureParagraph(table.Rows[rowIndex].Cells[4].Paragraphs.First(), result.FailureReason, 12, Alignment.center, fontFamily);
                rowIndex++;
            }

            return table;
        }

        private void InsertEmptyParagraph(DocX document, int parahraphNumber)
        {
            for (int i = 0; i < parahraphNumber; i++)
            {
                document.InsertParagraph();
            }
        }

        private String CreateReportFilePath(String fileName)
        {
            var pathReadyDateTime = DateTime.Now.ToString().Replace(':', '.');
            var reportFilesDirectoryPath = ConfigurationManager.AppSettings["ReportFilesDirectoryPath"] + "\\";

            var directory = new DirectoryInfo(reportFilesDirectoryPath);
            if (!directory.Exists) directory.Create();

            fileName = reportFilesDirectoryPath + String.Format(fileName, pathReadyDateTime);

            return fileName;
        }

        private void ConfigureParagraph(Paragraph paragraph, String text, double fontSize, Novacode.Alignment aligment,
            FontFamily fontFamily)
        {
            paragraph.Append(text);
            paragraph.FontSize(fontSize);
            paragraph.Font(fontFamily);
            paragraph.Alignment = aligment;
        }
    }
}
