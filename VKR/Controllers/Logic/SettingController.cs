﻿using System;
using System.Configuration;
using VKR.Controllers.Logic.Interfaces;

namespace VKR.Controllers.Logic
{
    public class SettingController : ISettingController
    {
        public string GetReportPath()
        {
            return ConfigurationManager.AppSettings["ReportFilesDirectoryPath"];
        }

        public string GetTestPath()
        {
            return ConfigurationManager.AppSettings["TestFilesDirectoryPath"];
        }

        public void SetReportPath(String value)
        {
            SetSetting("ReportFilesDirectoryPath", value);
        }

        public void SetTestPath(String value)
        {
            SetSetting("TestFilesDirectoryPath", value);
        }

        private static void SetSetting(string key, string value)
        {
            Configuration configuration =
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save(ConfigurationSaveMode.Full, true);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
