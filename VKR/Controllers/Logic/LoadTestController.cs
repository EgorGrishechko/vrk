﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using VKR.Controllers.Logic.Interfaces;
using VKR.Models.Test;

namespace VKR.Controllers.Logic
{
    public class LoadTestController : ILoadTestController
    {
        private const string ResourceName = "VKR.Documentation.TestExample.json";

        public async Task<List<UploadedTestModel>> LoadTests()
        {
            var testFilesDirectoryPath = ConfigurationManager.AppSettings["TestFilesDirectoryPath"];

            List<UploadedTestModel> parsedTests = null;

            var filePaths = await GetFilePathsAsync(testFilesDirectoryPath);

            if (filePaths != null)
            {
                var filesContent = await ReadFilesDataAsync(filePaths);
                parsedTests = ParseTestFiles(filesContent);
            }

            return parsedTests;
        }

        private async Task<List<string>> GetFilePathsAsync(string testFilesDirectoryPath)
        {
            List<string> readFilePaths = null;
            try
            {
                if (!Directory.Exists(testFilesDirectoryPath))
                {
                    await CreateTestDirectoryAsync(testFilesDirectoryPath);
                }

                readFilePaths = Directory.GetFiles(testFilesDirectoryPath, "*.json",
                        SearchOption.AllDirectories).ToList();
            }
            catch (Exception)
            {
                MessageBox.Show("Указанная папка с тестами не существует или её создание невозможно");
            }

            return readFilePaths;
        }

        private async Task CreateTestDirectoryAsync(string testFilesDirectoryPath)
        {
            Directory.CreateDirectory(testFilesDirectoryPath);
            var assembly = Assembly.GetExecutingAssembly();
            var testFilePath = Path.Combine(testFilesDirectoryPath, "TestExample.json");

            using (var stream = assembly.GetManifestResourceStream(ResourceName))
            {
                if (stream != null)
                {
                    using (var fileStream = new FileStream(testFilePath, FileMode.CreateNew))
                    {
                        await stream.CopyToAsync(fileStream);
                    }
                }
            }

        }

        private async Task<Dictionary<string, string>> ReadFilesDataAsync(List<string> filesPaths)
        {
            var readFiles = new Dictionary<string, string>();
            foreach (var filePath in filesPaths)
            {
                try
                {
                    using (var reader = new StreamReader(filePath, Encoding.Default))
                    {
                        var fileText = await reader.ReadToEndAsync();
                        readFiles.Add(filePath, fileText);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(String.Format("Невозможно прочитать файл - {0}", filePath));
                }

            }
            return readFiles;
        }

        private List<UploadedTestModel> ParseTestFiles(Dictionary<string, string> filesContent)
        {
            var resultList = new List<UploadedTestModel>();
            foreach (var testBody in filesContent)
            {
                try
                {
                    var currentCulture = Thread.CurrentThread.CurrentCulture;
                    var serializerSettings = new JsonSerializerSettings()
                    {
                        Culture = currentCulture
                    };

                    var parsedData = JsonConvert.DeserializeObject<UploadedTestModel>(testBody.Value, serializerSettings);
                    resultList.Add(parsedData);
                }
                catch (Exception)
                {
                    MessageBox.Show(String.Format("Невозможно загрузить файл в систему - {0}", testBody.Key));
                }

            }
            return resultList;
        }
    }
}
