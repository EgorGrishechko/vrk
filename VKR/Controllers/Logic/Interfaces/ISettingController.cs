﻿using System;

namespace VKR.Controllers.Logic.Interfaces
{
    public interface ISettingController
    {
        string GetReportPath();
        string GetTestPath();
        void SetReportPath(String value);
        void SetTestPath(String value);
    }
}