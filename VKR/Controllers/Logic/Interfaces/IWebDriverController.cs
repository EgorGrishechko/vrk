﻿using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using VKR.Models.WebDriver;

namespace VKR.Controllers.Logic.Interfaces
{
    public interface IWebDriverController
    {
        RemoteWebDriver WebDriver { get; }
        void OpenUrl(string url);
        IWebElement FindWebElement(WebItemModel webItemModel);
        string GetCurrentPageUrl();
        Task DelayAsync(int seconds);
    }
}