﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Models.Test;

namespace VKR.Controllers.Logic.Interfaces
{
    public interface ILoadTestController
    {
        Task<List<UploadedTestModel>> LoadTests();
    }
}