﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Controllers.Base.Interfaces;
using VKR.Database.Models;
using VKR.Models.Statistic;

namespace VKR.Controllers.Logic.Interfaces
{
    public interface IStatisticController : IBaseController
    {
        Task<StatisticModel> CalculateProbability(TestDbModel test);
        Task<IList<TestDbModel>> GetTestBySurveyType(string surveyType);
    }
}