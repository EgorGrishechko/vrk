﻿using System.Collections.Generic;
using VKR.Database.Models;

namespace VKR.Controllers.Logic.Interfaces
{
    public interface IReportController
    {
        void CreateTestReportByTestPerforming(IList<ResultDbModel> results);
        void CreateTestReportByDate(IList<ResultDbModel> results);
    }
}