﻿using System;
using System.Threading.Tasks;
using System.Windows.Controls;
using VKR.Controllers.Base.Interfaces;
using VKR.Models.Test;

namespace VKR.Controllers.Logic.Interfaces
{
    public interface IPerformTestController : IBaseController
    {
        Task PerfomTest(UploadedTestModel uploadedTestModel, StackPanel infoPanel, DateTime testingDate);
    }
}