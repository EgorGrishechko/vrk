﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ninject;
using Ninject.Parameters;
using VKR.Controllers.Base;
using VKR.Controllers.Data.Interfaces;
using VKR.Controllers.Logic.Interfaces;
using VKR.Database.Interfaces;
using VKR.Database.Models;
using VKR.IoC;
using VKR.Models.Statistic;
using VKR.Resources;

namespace VKR.Controllers.Logic
{
    public class StatisticController : BaseController, IStatisticController
    {
        private readonly IResultDbModelDataController _resultDbModelDataController;
        private readonly ITestDbModelDataController _testDbModelDataController;

        public StatisticController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _resultDbModelDataController = IoCContainer.Kernel.Get<IResultDbModelDataController>(new ConstructorArgument("unitOfWork", UnitOfWork));
            _testDbModelDataController = IoCContainer.Kernel.Get<ITestDbModelDataController>(new ConstructorArgument("unitOfWork", UnitOfWork));
        }

        public async Task<StatisticModel> CalculateProbability(TestDbModel test)
        {
            var results = await _resultDbModelDataController.GetByTestIdAsync(test.Id);

            var resultsNumber = results.Count;

            if (resultsNumber > 0)
            {
                var passedNumber = results.Count(s => s.Status == TestStatusEnum.Passed);

                var successPercent = (double) (100 / resultsNumber)*passedNumber;
                var failedPercent = 100 - successPercent;

                var statisticModel = new StatisticModel
                {
                    SuccessedPercent = successPercent,
                    FailedPercent = failedPercent,
                    FailedProbability = new Dictionary<string, double>()
                };

                var failedResults = results.Where(s => s.Status == TestStatusEnum.Failed).GroupBy(s => s.FailureReason);
                var failedNumber = resultsNumber - passedNumber;
                var onePercent = (double)100 / failedNumber;

                foreach (var failedGroup in failedResults)
                {
                    var reason = failedGroup.Key;
                    var percent = onePercent*failedGroup.Count();
                    statisticModel.FailedProbability.Add(reason, percent);
                }

                return statisticModel;
            }

            return null;
        }

        public async Task<IList<TestDbModel>> GetTestBySurveyType(string surveyType)
        {            
            IList<TestDbModel> tests = null;
            switch (surveyType)
            {
                case TestResultDictionary.All:
                    tests = await _testDbModelDataController.GetListAsync();
                    break;
                case TestResultDictionary.Successed:
                    tests = await _testDbModelDataController.GetSuccessedResultTestsAsync();                                                 
                    break;
                case TestResultDictionary.Failed:
                    tests = await _testDbModelDataController.GetFailedResultTestsAsync();   
                    break;
                case  TestResultDictionary.Cross:
                    tests = await _testDbModelDataController.GetCrossResultTestsAsync();   
                    break;
                default:
                    throw new Exception("Problems with survey type");
            }
            return tests;
        }
    }
}
