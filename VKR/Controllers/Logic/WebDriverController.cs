﻿using System;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using VKR.Controllers.Logic.Interfaces;
using VKR.Models.WebDriver;

namespace VKR.Controllers.Logic
{
    public class WebDriverController : IWebDriverController
    {
        private RemoteWebDriver _webDriver;

        public RemoteWebDriver WebDriver
        {
            get
            {
                if (_webDriver == null) _webDriver = new FirefoxDriver();
                return _webDriver;
            }
        }

        public void OpenUrl(string url)
        {
            WebDriver.Navigate().GoToUrl(url);
        }


        public IWebElement FindWebElement(WebItemModel webItemModel)
        {
            var wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(25));
            var clickableElement = wait.Until(ExpectedConditions.ElementExists(By.CssSelector(webItemModel.Id)));
            return clickableElement;
        }

        public string GetCurrentPageUrl()
        {
            var currentPageUrl = WebDriver.Url;
            return currentPageUrl;
        }

        public async Task DelayAsync(int seconds)
        {
            await Task.Delay(seconds * 1000);
        }
    }
}
