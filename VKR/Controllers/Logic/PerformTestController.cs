﻿using System;
using System.Threading.Tasks;
using System.Windows.Controls;
using Ninject;
using Ninject.Parameters;
using OpenQA.Selenium;
using VKR.Controllers.Base;
using VKR.Controllers.Data.Interfaces;
using VKR.Controllers.Logic.Interfaces;
using VKR.Controllers.UI;
using VKR.Database.Interfaces;
using VKR.IoC;
using VKR.Models.Test;
using VKR.Models.WebDriver;
using VKR.Resources;

namespace VKR.Controllers.Logic
{
    public class PerformTestController : BaseController, IPerformTestController
    {
        private UploadedTestModel _currentUploadedTestModel;

        private readonly IInfoStackPanelBuilder _infoStackPanelBuilder;
        private readonly IWebDriverController _webDriverController;
        private readonly IResultDbModelDataController _resultDbModelDataController;
        private readonly ITestDbModelDataController _testDbModelDataController;

        private StackPanel _infoPanel;
        private const string Done = " - Выполнено";

        public PerformTestController(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            _infoStackPanelBuilder = IoCContainer.Kernel.Get<IInfoStackPanelBuilder>();
            _webDriverController = IoCContainer.Kernel.Get<IWebDriverController>();
            _resultDbModelDataController = IoCContainer.Kernel.Get<IResultDbModelDataController>(new ConstructorArgument("unitOfWork", UnitOfWork));
            _testDbModelDataController = IoCContainer.Kernel.Get<ITestDbModelDataController>(new ConstructorArgument("unitOfWork", UnitOfWork));
        }

        public async Task PerfomTest(UploadedTestModel uploadedTestModel, StackPanel infoPanel, DateTime testingDate)
        {
            await _testDbModelDataController.InsertAsync(uploadedTestModel);

            _currentUploadedTestModel = uploadedTestModel;
            _infoPanel = infoPanel;

            var isFailed = false;
            var exceptionMessage = String.Empty;

            try
            {
                foreach (var testStep in _currentUploadedTestModel.Steps)
                {
                    switch (testStep.Action)
                    {
                        case TestActionDictionary.OpenPage:
                            await OpenPage(testStep);
                            _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, TestActionDictionary.OpenPage + Done);
                            break;
                        case TestActionDictionary.ElementExist:
                            await ElementExist(testStep);
                            _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, TestActionDictionary.ElementExist + Done);
                            break;
                        case TestActionDictionary.InputDataToElement:
                            await InputDataToElement(testStep);
                            _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, TestActionDictionary.InputDataToElement + Done);
                            break;
                        case TestActionDictionary.CompareElementValue:
                            await CompareElementValue(testStep);
                            _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, TestActionDictionary.CompareElementValue + Done);
                            break;
                        case TestActionDictionary.ClickOnElement:
                            await ClickOnTheElement(testStep);
                            _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, TestActionDictionary.ClickOnElement + Done);
                            break;
                        case TestActionDictionary.CheckPageUrl:
                            await CheckPageUrl(testStep);
                            _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, TestActionDictionary.CheckPageUrl + Done);
                            break;
                        default:
                            throw new Exception("Не известное действие");
                    }
                }

                _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, String.Format("\nId - {0}\n Имя - {1}.\n Выполнение закончено. Ошибок не выявлено.", _currentUploadedTestModel.Id, _currentUploadedTestModel.Name));
                await _resultDbModelDataController.InsertAsync(uploadedTestModel, true, String.Empty, testingDate);
            }
            catch (Exception ex)
            {
                isFailed = true;
                exceptionMessage = ex.Message;

                _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, exceptionMessage);
                _infoStackPanelBuilder.AddTextBlockToInfoPanel(_infoPanel, String.Format("Id - {0}, Имя - {1}.\n Выполнение закончено. Найдены ошибки.", _currentUploadedTestModel.Id, _currentUploadedTestModel.Name));
            }

            if (isFailed)
            {
                await _resultDbModelDataController.InsertAsync(uploadedTestModel, false, exceptionMessage, testingDate);
            }
        }

        private async Task WaitBeforeAsync(UploadedTestStepModel uploadedTestStepModel)
        {
            if (uploadedTestStepModel.WaitBefore.HasValue) await _webDriverController.DelayAsync(uploadedTestStepModel.WaitBefore.Value);            
        }

        private async Task WaitAfterAsync(UploadedTestStepModel uploadedTestStepModel)
        {
            if (uploadedTestStepModel.WaitAfter.HasValue) await _webDriverController.DelayAsync(uploadedTestStepModel.WaitAfter.Value);   
        }

        private IWebElement GetElement(UploadedTestStepModel currentUploadedTestStepModel)
        {
            var webItem = new WebItemModel(currentUploadedTestStepModel.ElementCssSelector, "", "");
            var element = _webDriverController.FindWebElement(webItem);

            if (element == null) throw new Exception(String.Format("Элемента с таким селектором не существует - {0}", currentUploadedTestStepModel.ElementCssSelector));

            return element;
        }

        private async Task OpenPage(UploadedTestStepModel currentUploadedTestStepModel)
        {
            await WaitBeforeAsync(currentUploadedTestStepModel);
            await Task.Run(() => _webDriverController.OpenUrl(currentUploadedTestStepModel.ElementValue));
            await WaitAfterAsync(currentUploadedTestStepModel);
        }

        private async Task ElementExist(UploadedTestStepModel currentUploadedTestStepModel)
        {
            await WaitBeforeAsync(currentUploadedTestStepModel);
            await Task.Run(() => GetElement(currentUploadedTestStepModel));
            await WaitAfterAsync(currentUploadedTestStepModel);
        }

        private async Task InputDataToElement(UploadedTestStepModel currentUploadedTestStepModel)
        {
            await WaitBeforeAsync(currentUploadedTestStepModel);
            var element = await Task.Run(() => GetElement(currentUploadedTestStepModel));
            element.SendKeys(currentUploadedTestStepModel.ElementValue);
            await WaitAfterAsync(currentUploadedTestStepModel);
        }

        private async Task CompareElementValue(UploadedTestStepModel currentUploadedTestStepModel)
        {
            await WaitBeforeAsync(currentUploadedTestStepModel);
            var element = await Task.Run(() => GetElement(currentUploadedTestStepModel));
            var currentValue = element.GetCssValue("Value");
            if (currentValue != currentUploadedTestStepModel.ElementValue)
            {
                throw new Exception(String.Format("Значения не равны {0} и {1}", currentValue, currentUploadedTestStepModel.ElementValue));
            }
            await WaitAfterAsync(currentUploadedTestStepModel);
        }

        private async Task ClickOnTheElement(UploadedTestStepModel currentUploadedTestStepModel)
        {
            await WaitBeforeAsync(currentUploadedTestStepModel);
            var element = await Task.Run(() => GetElement(currentUploadedTestStepModel));
            element.Click();
            await WaitAfterAsync(currentUploadedTestStepModel);
        }

        private async Task CheckPageUrl(UploadedTestStepModel currentUploadedTestStepModel)
        {
            await WaitBeforeAsync(currentUploadedTestStepModel);
            var pageUrl = _webDriverController.GetCurrentPageUrl();
            if (pageUrl != currentUploadedTestStepModel.ElementValue)
            {
                throw new Exception(String.Format("Адреса страниц не совпадают {0} и {1}", pageUrl, currentUploadedTestStepModel.ElementValue));
            }
            await WaitAfterAsync(currentUploadedTestStepModel);
        }
    }
}
