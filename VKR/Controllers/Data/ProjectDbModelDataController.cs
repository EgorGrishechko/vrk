﻿using System.Threading.Tasks;
using VKR.Controllers.Data.Interfaces;
using VKR.Database.Interfaces;
using VKR.Database.Models;

namespace VKR.Controllers.Data
{
    public class ProjectDbModelDataController : BaseDataController<IProjectDbModelRepository, ProjectDbModel>, IProjectDbModelDataController
    {
        public ProjectDbModelDataController(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.ProjectDbModelRepository) { }
 
        public async Task<ProjectDbModel> GetByUrlAsync(string url)
        {
            return await Repository.GetByUrlAsync(url);
        }

    }
}
