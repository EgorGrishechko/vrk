﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Database.Models;
using VKR.Models.Test;

namespace VKR.Controllers.Data.Interfaces
{
    public interface IResultDbModelDataController : IBaseDataController<ResultDbModel>
    {
        Task InsertAsync(UploadedTestModel uploadTestModel, Boolean isPassed, String failureReason, DateTime testingDate);
        Task<IList<ResultDbModel>> GetByTestIdAsync(Guid id);
        Task<ResultDbModel> GetLastResultAsync();
        Task<IList<TimeSpan>> GetResultTimesByDateWithOutTimeAsync(DateTime date);
        Task<IList<ResultDbModel>> GetResultsByDateTimeAsync(DateTime date);
    }
}
