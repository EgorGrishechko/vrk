﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Database.Models;
using VKR.Models.Test;

namespace VKR.Controllers.Data.Interfaces
{
    public interface ITestDbModelDataController : IBaseDataController<TestDbModel>
    {
        Task InsertAsync(UploadedTestModel uploadedTestModel);
        Task<IList<TestDbModel>> GetSuccessedResultTestsAsync();
        Task<IList<TestDbModel>> GetFailedResultTestsAsync();
        Task<IList<TestDbModel>> GetCrossResultTestsAsync();
    }
}