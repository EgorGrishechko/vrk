﻿using System.Threading.Tasks;
using VKR.Database.Models;

namespace VKR.Controllers.Data.Interfaces
{
    public interface IProjectDbModelDataController : IBaseDataController<ProjectDbModel>
    {
        Task<ProjectDbModel> GetByUrlAsync(string url);
    }
}