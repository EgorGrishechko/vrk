﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Controllers.Base.Interfaces;

namespace VKR.Controllers.Data.Interfaces
{
    public interface IBaseDataController<TEntity> : IBaseController
        where TEntity : class 
    {
        Task<TEntity> GetByIdAsync(Guid id);
        Task InsertAsync(TEntity obj);
        Task RemoveAsync(Guid id);
        Task<IList<TEntity>> GetListAsync();
    }
}
