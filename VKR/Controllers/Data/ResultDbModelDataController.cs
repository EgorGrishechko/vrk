﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using VKR.Controllers.Data.Interfaces;
using VKR.Database.Interfaces;
using VKR.Database.Models;
using VKR.Models.Test;
using VKR.Resources;

namespace VKR.Controllers.Data
{
    public class ResultDbModelDataController : BaseDataController<IResultDbModelRepository,ResultDbModel>, IResultDbModelDataController
    {
        public ResultDbModelDataController(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.ResultDbModelRepository) { }

        public async Task InsertAsync(UploadedTestModel uploadTestModel, Boolean isPassed, String failureReason, DateTime testingDate)
        {
            var savedTest = await UnitOfWork.TestDbModelRepository.GetByIdAsync(uploadTestModel.Id.Value);

            var resultDbModel = new ResultDbModel { Date = testingDate, Test = savedTest, Id = Guid.NewGuid()};

            if (!isPassed)
            {
                resultDbModel.FailureReason = failureReason;
                resultDbModel.Status = TestStatusEnum.Failed;
            }
            else
            {
                resultDbModel.Status = TestStatusEnum.Passed;
            }

            await Repository.InsertAsync(resultDbModel);
        }

        public async Task<IList<ResultDbModel>> GetByTestIdAsync(Guid id)
        {
            var result = await Repository.GetByTestIdAsync(id);
            return result;
        }

        public async Task<ResultDbModel> GetLastResultAsync()
        {
            var result = await Repository.GetLastResultAsync();
            return result;
        }


        public async Task<IList<TimeSpan>> GetResultTimesByDateWithOutTimeAsync(DateTime date)
        {
            var results = await Repository.GetResultsByDateWithoutTimeAsync(date);
            var times = results.Select(t => t.Date.TimeOfDay).Distinct().ToList();
            return times;
        }

        public async Task<IList<ResultDbModel>> GetResultsByDateTimeAsync(DateTime date)
        {
            var result = await Repository.GetResultsByDateTimeAsync(date);
            return result;
        }
    }
}
