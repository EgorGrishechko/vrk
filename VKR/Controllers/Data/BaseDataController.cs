﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Controllers.Base;
using VKR.Controllers.Data.Interfaces;
using VKR.Database.Interfaces;

namespace VKR.Controllers.Data
{
    public abstract class BaseDataController<TRepository, TEntity> :BaseController, IBaseDataController<TEntity>, IDisposable
        where TEntity : class 
        where TRepository : IBaseRepository<TEntity>
    {
        private readonly TRepository _repository;

        protected TRepository Repository
        {
            get { return _repository; }
        }

        protected BaseDataController(IUnitOfWork unitOfWork, TRepository repository) : base(unitOfWork)
        {
            _repository = repository;
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public async Task InsertAsync(TEntity obj)
        {
            await _repository.InsertAsync(obj);
        }

        public async Task RemoveAsync(Guid id)
        {
            await _repository.RemoveAsync(id);
        }

        public async Task<IList<TEntity>> GetListAsync()
        {
            return await _repository.GetListAsync();
        }
    }
}
