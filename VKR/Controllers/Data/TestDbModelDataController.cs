﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VKR.Controllers.Data.Interfaces;
using VKR.Database.Interfaces;
using VKR.Database.Models;
using VKR.Models.Test;

namespace VKR.Controllers.Data
{
    public class TestDbModelDataController : BaseDataController<ITestDbModelRepository, TestDbModel>, ITestDbModelDataController
    {
        public TestDbModelDataController(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.TestDbModelRepository) {}

        public async Task InsertAsync(UploadedTestModel uploadedTestModel)
        {
            var savedTest = await Repository.GetByIdAsync(uploadedTestModel.Id.Value);
            if (savedTest == null)
            {
                var testDbModel = new TestDbModel
                {
                    Id = uploadedTestModel.Id.Value,
                    Name = uploadedTestModel.Name,
                    Date = uploadedTestModel.Date.HasValue ? uploadedTestModel.Date.Value : DateTime.Now
                };

                var project = await UnitOfWork.ProjectDbModelRepository.GetByUrlAsync(uploadedTestModel.Steps[0].ElementValue);

                if (project == null)
                {
                    project = new ProjectDbModel()
                    {
                        Url = uploadedTestModel.Steps[0].ElementValue,
                        Id = Guid.NewGuid()
                    };
                }
                testDbModel.Project = project;
                await Repository.InsertAsync(testDbModel);
            }
        }

        public async Task<IList<TestDbModel>> GetSuccessedResultTestsAsync()
        {
            var tests = await Repository.GetSuccessedResultTestsAsync();;
            return tests;
        }

        public async Task<IList<TestDbModel>> GetFailedResultTestsAsync()
        {
            var tests = await Repository.GetFailedResultTestsAsync();
            return tests;
        }

        public async Task<IList<TestDbModel>> GetCrossResultTestsAsync()
        {
            var tests = await Repository.GetCrossResultTestsAsync();
            return tests;
        }
    }
}
