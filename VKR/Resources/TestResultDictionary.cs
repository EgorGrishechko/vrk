﻿namespace VKR.Resources
{
    public class TestResultDictionary
    {
        public const string All = "Все результаты";
        public const string Successed = "Только успешные";
        public const string Failed = "Только проваленные";
        public const string Cross = "Бывшие и проваленными и выполненными";
    }
}
