﻿namespace VKR.Resources
{
    public class TestActionDictionary
    {
        public const string OpenPage = "Открыть страницу";
        public const string ElementExist = "Элемент существует";
        public const string ClickOnElement = "Щелкнуть на элемент";
        public const string InputDataToElement = "Заполнить элемент";
        public const string CompareElementValue = "Сравнить значение элемента";
        public const string CheckPageUrl = "Проверить адрес страницы";
    }
}
