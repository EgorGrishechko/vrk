# README #

### What is this repository for? ###

This is a repository for my final project in the university. I've created it for showing my level of programming and for improving my skills in using EF and DI.

This is a simple UI-manager for running Selenium tests.

### How do I get set up? ###

This project is using a local database. You need only .net framework 4.5. Other stuff will be downloaded with nuget.