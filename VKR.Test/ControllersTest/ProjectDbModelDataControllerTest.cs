﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using VKR.Controllers.Data;
using VKR.Database.Interfaces;
using VKR.Database.Models;

namespace VKR.Test.ControllersTest
{
    [TestFixture]
    public class ProjectDbModelDataControllerTest
    {
        [Test]
        public async Task GetByUrlAsyncTest()
        {
            //Arrange
            var projectDbModelId = Guid.NewGuid();
            var projectDbModelUrl = "TestUrl";

            var expectedDmModel = new ProjectDbModel()
            {
                Id = projectDbModelId,
                Url = projectDbModelUrl
            };

            var iUnitOfWorkMock = new Mock<IUnitOfWork>();

            var projectRepositoryMock = new Mock<IProjectDbModelRepository>();
            projectRepositoryMock.Setup(r => r.GetByUrlAsync(It.IsAny<string>())).ReturnsAsync(expectedDmModel);

            iUnitOfWorkMock.Setup(iu => iu.ProjectDbModelRepository).Returns(projectRepositoryMock.Object);

            // Act
            var projectDbModelDataController = new ProjectDbModelDataController(iUnitOfWorkMock.Object);
            var result = await projectDbModelDataController.GetByUrlAsync(projectDbModelUrl);

            //Assert
            Assert.AreEqual(result.Id, projectDbModelId);
            Assert.AreEqual(result.Url, projectDbModelUrl);
        }

        [Test]
        [ExpectedException]
        public async Task GetByUrlAsyncWithExceptionTest()
        {
            //Arrange
            var iUnitOfWorkMock = new Mock<IUnitOfWork>();

            var projectRepositoryMock = new Mock<IProjectDbModelRepository>();
            projectRepositoryMock.Setup(r => r.GetByUrlAsync(It.IsAny<string>()))
                .Throws(new Exception("ExpectedException"));

            iUnitOfWorkMock.Setup(iu => iu.ProjectDbModelRepository).Returns(projectRepositoryMock.Object);

            // Act
            var projectDbModelDataController = new ProjectDbModelDataController(iUnitOfWorkMock.Object);
            await projectDbModelDataController.GetByUrlAsync(string.Empty);
        }
    }
}
