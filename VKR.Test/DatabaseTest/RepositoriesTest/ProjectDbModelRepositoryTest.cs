﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using VKR.Database.Context;
using VKR.Database.Models;
using VKR.Database.Repositories;



namespace VKR.Test.DatabaseTest.RepositoriesTest
{
    [TestFixture]
    public class ProjectDbModelRepositoryTest
    {
        private Mock<DbSet<ProjectDbModel>> CreateMockDbSet(IQueryable<ProjectDbModel> data)
        {

            var mockSet = new Mock<DbSet<ProjectDbModel>>();
            mockSet.As<IDbAsyncEnumerable<ProjectDbModel>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<ProjectDbModel>(data.GetEnumerator()));

            mockSet.As<IQueryable<ProjectDbModel>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<ProjectDbModel>(data.Provider));

            mockSet.As<IQueryable<ProjectDbModel>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<ProjectDbModel>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<ProjectDbModel>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            return mockSet;
        }

        [Test]
        public async Task GetByUrlAsyncTest()
        {
            //Arrange
            var expectedId = Guid.NewGuid();
            var expetedUrl = "ExpectedUrl";

            var data = new List<ProjectDbModel> 
            { 
                new ProjectDbModel {Id = expectedId, Url = expetedUrl }, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = string.Empty }, 
                new ProjectDbModel { Id = Guid.NewGuid()}, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = "UnExpectedUrl" }, 
            }.AsQueryable();

            var mockSet = CreateMockDbSet(data);

            var mockContext = new Mock<DbContext>();
            mockContext.Setup(c => c.Set<ProjectDbModel>()).Returns(mockSet.Object);

            //Act
            var repository = new ProjectDbModelRepository(mockContext.Object);
            var projectDbModel = await repository.GetByUrlAsync(expetedUrl);

            //Assert
            Assert.IsNotNull(projectDbModel);
            Assert.AreEqual(projectDbModel.Id, expectedId);
            Assert.AreEqual(projectDbModel.Url, expetedUrl); 
        }



        // Testing base controller features

        [Test]
        public async Task GetByIdAsyncBaseTest()
        {
            //Arrange
            var expectedId = Guid.NewGuid();
            var expetedUrl = "ExpectedUrl";

            var data = new List<ProjectDbModel> 
            { 
                new ProjectDbModel {Id = expectedId, Url = expetedUrl }, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = string.Empty }, 
            }.AsQueryable();

            var mockSet = CreateMockDbSet(data);

            var mockContext = new Mock<DbContext>();
            mockContext.Setup(c => c.Set<ProjectDbModel>()).Returns(mockSet.Object);

            //Act
            var repository = new ProjectDbModelRepository(mockContext.Object);
            var projectDbModel = await repository.GetByIdAsync(expectedId);

            //Assert
            Assert.IsNotNull(projectDbModel);
            Assert.AreEqual(projectDbModel.Id, expectedId);
        }

        [Test]
        public async Task GetByIdAsyncBaseWithEmptyDataTest()
        {
            //Arrange
            var data = new List<ProjectDbModel>().AsQueryable();
            var mockSet = CreateMockDbSet(data);

            var mockContext = new Mock<DbContext>();
            mockContext.Setup(c => c.Set<ProjectDbModel>()).Returns(mockSet.Object);

            //Act
            var repository = new ProjectDbModelRepository(mockContext.Object);
            var projectDbModel = await repository.GetByIdAsync(Guid.NewGuid());

            //Assert
            Assert.IsNull(projectDbModel);
        }

        [Test]
        public async Task GetListAsyncBaseTest()
        {
            //Arrange
            var data = new List<ProjectDbModel> 
            { 
                new ProjectDbModel { Id  = Guid.NewGuid(), Url = "1" }, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = "2" }, 
                new ProjectDbModel { Id = Guid.NewGuid()}, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = "3" }, 
            }.AsQueryable();


            var mockSet = CreateMockDbSet(data);

            var mockContext = new Mock<DbContext>();
            mockContext.Setup(c => c.Set<ProjectDbModel>()).Returns(mockSet.Object);

            //Act
            var repository = new ProjectDbModelRepository(mockContext.Object);
            var projectDbModels = await repository.GetListAsync();

            //Assert
            Assert.IsNotNull(projectDbModels);
            Assert.IsNotEmpty(projectDbModels);
            Assert.AreEqual(projectDbModels.Count, 4);
        }


        [Test]
        public async Task GetListAsyncBaseWithEmptyDataTest()
        {
            //Arrange
            var data = new List<ProjectDbModel>().AsQueryable();
            var mockSet = CreateMockDbSet(data);

            var mockContext = new Mock<DbContext>();
            mockContext.Setup(c => c.Set<ProjectDbModel>()).Returns(mockSet.Object);

            //Act
            var repository = new ProjectDbModelRepository(mockContext.Object);
            var projectDbModels = await repository.GetListAsync();

            //Assert
            Assert.IsEmpty(projectDbModels);
        }

        [Test]
        public async Task SearchBaseTest()
        {
            //Arrange
            var data = new List<ProjectDbModel> 
            { 
                new ProjectDbModel { Id  = Guid.NewGuid(), Url = "1" }, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = "2" }, 
                new ProjectDbModel { Id = Guid.NewGuid()}, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = "3" }, 
            }.AsQueryable();


            var mockSet = CreateMockDbSet(data);

            var mockContext = new Mock<DbContext>();
            mockContext.Setup(c => c.Set<ProjectDbModel>()).Returns(mockSet.Object);

            //Act
            var repository = new ProjectDbModelRepository(mockContext.Object);

            var firstModel = repository.Search(p => p.Url == "3").FirstOrDefault();
            var secondModel = repository.Search(p => String.IsNullOrEmpty(p.Url)).FirstOrDefault();
            var thirdModels = repository.Search(p => !String.IsNullOrEmpty(p.Url));

            //Assert
            Assert.IsNotNull(firstModel);
            Assert.IsNotNull(secondModel);
            Assert.IsNotNull(thirdModels);
            Assert.IsTrue(thirdModels is IQueryable);
            Assert.AreEqual(firstModel.Url, "3");
            Assert.AreEqual(secondModel.Url, null);
            Assert.AreEqual(thirdModels.Count(), 3);
        }

        [Test]
        public void GetAllBaseTest()
        {
            //Arrange
            var data = new List<ProjectDbModel> 
            { 
                new ProjectDbModel { Id  = Guid.NewGuid(), Url = "1" }, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = "2" }, 
                new ProjectDbModel { Id = Guid.NewGuid()}, 
                new ProjectDbModel { Id = Guid.NewGuid(), Url = "3" }, 
            }.AsQueryable();


            var mockSet = CreateMockDbSet(data);

            var mockContext = new Mock<DbContext>();
            mockContext.Setup(c => c.Set<ProjectDbModel>()).Returns(mockSet.Object);

            //Act
            var repository = new ProjectDbModelRepository(mockContext.Object);

            var result = repository.GetAll();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), 4);
        }

    }
}
